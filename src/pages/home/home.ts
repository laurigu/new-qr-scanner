import { Component } from '@angular/core';
import { LoadingController, NavController } from 'ionic-angular';

declare const QRScanner: any;

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    constructor(public navCtrl: NavController, public loadingCtrl: LoadingController) {
        // Start a scan. Scanning will continue until something is detected or
        // `QRScanner.cancelScan()` is called.
        QRScanner.scan(displayContents);

        let loader = this.loadingCtrl.create({content: '', dismissOnPageChange: true});

        function displayContents(error, text) {
            loader.present();
            if (error) {
                // an error occurred, or the scan was canceled (error code `6`)
                console.log('error', error);
            } else {
                // The scan completed, display the contents of the QR code:
                console.log('success', text);
            }

            //Redirect to the result page based on the result
            //HIDE THE PREVIEW (else the app will lag)
            //Potentially show a loader till the http call is done?
            setTimeout(() => {
                QRScanner.hide(function(status){
                    console.log(status);
                    loader.dismiss();
                });
            }, 3000)
        }

        // Make the webview transparent so the video preview is visible behind it.
        QRScanner.show();
        // Be sure to make any opaque HTML elements transparent here to avoid
        // covering the video.
    }

}
